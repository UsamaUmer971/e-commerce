import axios from "axios";
import {
    ALL_PRODUCT_FAIL,
    ALL_PRODUCT_REQUEST,
    ALL_PRODUCT_SUCCESS,
    CLEAR_ERRORS
}from "../constants/productConstant";
// Get Product
export const getProduct=()=>async(dispatch)=>{
    try {
        dispatch({type : ALL_PRODUCT_REQUEST});

        const data= await axios.get('/app/products');

        dispatch({
            type:ALL_PRODUCT_SUCCESS,
            payload:data,
        })
        
    } catch (error) {
        dispatch({
           type: ALL_PRODUCT_FAIL,
           payload:error.response.data.message,
        })
    }
}

// Clearing Errors
export const clearErrors = () => async (dispatch) => {
    dispatch({ type: CLEAR_ERRORS });
  };

// 192.168.0.113  