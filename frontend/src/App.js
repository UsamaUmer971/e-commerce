import './App.css';
import Header from "./components/layout/Header/Header.js"
import Footer from "./components/layout/Footer/Footer"
import {BrowserRouter as Router,Route} from "react-router-dom";
import WebFont from 'webfontloader';
import React from 'react';
import Home from './components/Home/Home.js'


function App() {

  
React.useEffect(()=>{
  WebFont.load({
    google:{
      families:["Poppins","Roboto"]
    }
  })
},[]);

  return (
    <Router>
      <Header/>
      <Route exact path="/" component={Home}/>
      <Footer/>
    </Router>
  );
}

export default App;
