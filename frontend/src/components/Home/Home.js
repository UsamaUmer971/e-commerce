import React, { Fragment,useEffect } from 'react'
import { CgMouse } from "react-icons/all";
import "./Home.css";
import Product from "./ProductCard.js"
import MetaData from '../layout/MetaData';
import { getProduct } from '../../actions/productAction';
import { useSelector, useDispatch } from "react-redux"; 



// Dummy Product
// const product={
//   name:"Shirt",
//   images:[{ url:"https://cdn.pixabay.com/photo/2016/11/23/06/57/isolated-t-shirt-1852114_960_720.png"}],
//   price:"200",
//   _id:"shirt"
// }

const Home = () => {

  const dispatch = useDispatch();
  const {loading, error, products,productsCount}=useSelector(state=>state.products)

  useEffect(() => {
    dispatch(getProduct());
  }, [dispatch]);
    return <Fragment>
      <MetaData title="Ecommerce"/>

    <div className="banner">
            <p>Welcome to Ecommerce</p>
            <h1>FIND AMAZING PRODUCTS BELOW</h1>

            <a href="#container">
              <button>
                Scroll <CgMouse />
              </button>
            </a>
    </div>
    <h2 className="homeHeading">Featured Products</h2>
    <div className="container" id="container">
        {products && products.map(product=>(
            <Product product={product} />
        ))}
          
    </div>
    </Fragment>
}

export default Home
